#ifndef WIFI_HELPER_H
#define WIFI_HELPER_H

#include <lwip_netconf.h>
#include <lwip/netif.h>
#include <lwip/sockets.h>
#include "google_nest.h"
#include "cJSON.h"
#include "main.h"
#include "semphr.h"

#define BUF_SIZE 40
#define TASK_INTERVAL 300
#define SOCKET_TYPE 1 // 0 means TCP, else UDP
#define NTP_PACKET_SIZE 48

#define FIREBASE_HOST_ADDR       "brilliant-fire-5010.firebaseio.com"
#define FIREBASE_PORT		443

extern void sensorTask();
extern void clntTask();

void connectWiFiSema(void *wifiReadySema);
void connectWiFi(void *param);
void initAP(void *param);
int initAddrIn(char *ip, unsigned short port, struct sockaddr_in *addrIn);
int createSocket(unsigned short type, struct sockaddr_in *addrIn);
int connectToServer(int fd, unsigned short type, struct sockaddr_in *addrIn);
int sendData(int fd, unsigned short type, const void *buf, int buf_size, struct sockaddr_in *addrIn);
int uploadSensorData(const void *buf, int buf_size);
int sendNTPpacket();
void sendToFirebase(sensor_buffer *buf);
#endif
