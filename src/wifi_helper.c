#include "FreeRTOS.h"
#include "task.h"
#include "device.h"

#include "wifi_conf.h"
#include "wlan_intf.h"
#include "lwip_netconf.h"
#include "wifi_constants.h"
#include "wifi_util.h"

#include "wifi_helper.h"

const char *ip = "10.10.10.100";
unsigned short port = 1982;
char *ssid = "78737B";
char *pw = "00000aaaaa";
char *ap_ssid = "REALTEK_AMEBA";
char *ap_pw = "00000aaaaa";
char *ap_ch = "6";
unsigned short tcp_clnt_terminate = 0;
static struct sockaddr_in sAddr;

extern struct netif xnetif[NET_IF_NUM];
extern void do_ping_test();

void connectWiFiSema(void *wifiReadySema) {
  rtl_printf("before trying wifi_connect\n");
  wifi_off();

  if (wifi_on(RTW_MODE_STA) < 0){
     DiagPrintf("\n\rERROR: Wifi on failed!");
  }


  int ret = wifi_connect( ssid, // SSID
                          RTW_SECURITY_WPA2_AES_PSK, // Securrity
                          pw,// Password
                          strlen(ssid),// SSID len
                          strlen(pw),// password len
                          -1,// key id
                          wifiReadySema // NULL
                          );

  if(ret!= RTW_SUCCESS){
    DiagPrintf("\n\rERROR: Operation failed!");
  }
  LwIP_DHCP(0, DHCP_START);
  DiagPrintf("\n\rBefore sending semaphore");
  xSemaphoreGive(*((xSemaphoreHandle *)wifiReadySema));
  vTaskDelete(NULL);
}

void connectWiFi(void *param) {
  DiagPrintf("entering connect_wifi \n\r");
  // wifi_off();

  // __u8 mode;
  // wifi_get_autoreconnect(&mode);
  // printf("auto reconnect %d\n", mode);

  if (wifi_on(RTW_MODE_STA) < 0){
    DiagPrintf("\n\rERROR: Wifi on failed!");
  }
  // wifi_enable_powersave();
  fATW0(ssid);
  fATW1(pw);
  fATWC(NULL);
  printf("wifi connected\n");

  // xSemaphoreGive(param);
  vTaskDelete(NULL);
}

void initAP(void *param) {
    DiagPrintf("entering initAP \n\r");

    if (wifi_on(RTW_MODE_STA_AP) < 0){
        DiagPrintf("\n\rERROR: Wifi on failed!");
    }
    DiagPrintf("before connect\n");
    fATW3(ap_ssid);
    fATW4(ap_pw);
    fATW5(ap_ch);
    fATWB(NULL);
    DiagPrintf("wifi ap initialized\n");
    dhcps_init(&xnetif[1]);
    vTaskDelete(NULL);
}

void pingServer() {
  do_ping_call(ip, 0, 5);
}

int initAddrIn(char *ip, unsigned short port, struct sockaddr_in *addrIn) {
  if (ip == NULL || addrIn == NULL) return -1;

  //filling the TCP server socket address
  addrIn->sin_family = AF_INET;
  addrIn->sin_port = htons(port);
  addrIn->sin_addr.s_addr = inet_addr(ip);

  return 0;
}

/**
 * @Return: client socket fd
 */
int createSocket(unsigned short type, struct sockaddr_in *addrIn) {
  if (addrIn == NULL) return -1;

  int iSockFD;
  // creating a TCP socket
  iSockFD = socket(AF_INET, (type)? SOCK_DGRAM: SOCK_STREAM, 0);
  if( iSockFD < 0 ) {
  	printf("\n\rTCP ERROR: create tcp client socket fd error!");
  	return -1;
  }
  printf("\n\r Create socket %d.", iSockFD);
  return iSockFD;
}

int connectToServer(int fd, unsigned short type, struct sockaddr_in *addrIn) {
  if (addrIn == NULL) return -1;

  // connecting to TCP server
  int iStatus = connect(fd, (struct sockaddr *)addrIn, sizeof(struct sockaddr_in));
  if (iStatus < 0) {
  	printf("\n\rTCP ERROR: tcp client connect server error! %d, fd %d\n", iStatus, fd);
    close(fd);
  	return iStatus;
  }

  printf("\n\rTCP: Connect to server %x successfully.", addrIn->sin_addr.s_addr);
  return 0;
}

int sendData(int fd, unsigned short type, const void *buf, int buf_size, struct sockaddr_in *addrIn) {
  int iStatus = 1;
  if (!type) iStatus = send(fd, buf, buf_size, 0);
  else iStatus = sendto(fd, buf, buf_size, 0, (struct sockaddr *)addrIn, sizeof(struct sockaddr_in));

  return iStatus;
}

/**
 * @brief: This will be called every 60 secs.
 * @Return: status
 */
int uploadSensorData(const void *buf, int buf_size) {
  if (buf == NULL) return -1;

  // fd is the indication on wheither a new socket is needed.
  static int fdSensor = -1;
  int iStatus = 0;
  struct sockaddr_in servAddr;

  initAddrIn(ip, port, &servAddr);

  if(fdSensor < 0) {
    fdSensor = createSocket(SOCKET_TYPE, &servAddr);
    if (!SOCKET_TYPE) iStatus = connectToServer(fdSensor, SOCKET_TYPE, &servAddr);
    else iStatus = 0;
  }

  if(iStatus < 0) {
    fdSensor = -1;
    return iStatus;
  }

  iStatus = sendData(fdSensor, SOCKET_TYPE, buf, buf_size, &servAddr);
  if( iStatus < 0 ) {
    printf("\r\nTCP ERROR: tcp client send data error!  iStatus:%d\n", iStatus);
    close(fdSensor);
    fdSensor = -1;
    return iStatus;
  }
  return iStatus;
}

int sendNTPpacket() {
  // if (packetBuffer == NULL || packetBuffer[NTP_PACKET_SIZE-1] == NULL) return -1;
  unsigned char packetBuffer[NTP_PACKET_SIZE];
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0xE3;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // time-c.nist.gov
  char _ip[20] = "129.6.15.30";
  struct sockaddr_in servAddr;
  int fdNTP = -1;
  int iStatus = 0;
  int addrLen = sizeof(struct sockaddr_in);

  initAddrIn(_ip, 123, &servAddr);

  if(fdNTP < 0) {
    fdNTP = createSocket(1, &servAddr);
  }

  iStatus = sendData(fdNTP, 1, packetBuffer, NTP_PACKET_SIZE, &servAddr);
  if( iStatus < 0 ) {
    printf("\r\nclient send data error!  iStatus:%d\n", iStatus);
    close(fdNTP);
    fdNTP = -1;
    return iStatus;
  }

  int n = recvfrom(fdNTP, packetBuffer, NTP_PACKET_SIZE, 0, &servAddr, &addrLen);
  close(fdNTP);
  packetBuffer[n] = 0;

  if (n < 44) return -1;
  //the timestamp starts at byte 40 of the received packet and is four bytes,
  // or two words, long. First, esxtract the two words:
  unsigned long res = packetBuffer[40] << 24 |
                      packetBuffer[41] << 16 |
                      packetBuffer[42] << 8  |
                      packetBuffer[43];

  // now convert NTP time into everyday time:
  // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
  const unsigned long seventyYears = 2208988800UL;
  // subtract seventy years:
  unsigned long epoch = res - seventyYears;

  // print the hour, minute and second:
  printf("The UTC time is %d: %d: %d\n", (epoch%86400L)/3600,
                                         (epoch%3600)/60,
                                          epoch%60);
  return iStatus;
}

void sendToFirebase(sensor_buffer *buf) {
	googlenest_context googlenest;
	char *googlenest_uri = "Sensor.json";
	cJSON_Hooks memoryHook;
  int j = 0;

	memoryHook.malloc_fn = malloc;
	memoryHook.free_fn = free;
	cJSON_InitHooks(&memoryHook);
	printf("\r\nStart connecting to Google Nest Server!\r\n");

	while(1) {
		memset(&googlenest, 0, sizeof(googlenest_context));
		if(gn_connect(&googlenest, FIREBASE_HOST_ADDR, FIREBASE_PORT) == 0) {
			cJSON *MSJSObject;
			char *data;

			if((MSJSObject = cJSON_CreateObject()) != NULL) {
					cJSON_AddItemToObject(MSJSObject, "temperature", cJSON_CreateNumber(buf->temp));
          cJSON_AddItemToObject(MSJSObject, "humidity", cJSON_CreateNumber(buf->humidity));
          cJSON_AddItemToObject(MSJSObject, "accX", cJSON_CreateNumber(buf->accX));
          cJSON_AddItemToObject(MSJSObject, "accY", cJSON_CreateNumber(buf->accY));
          cJSON_AddItemToObject(MSJSObject, "accZ", cJSON_CreateNumber(buf->accZ));
          cJSON_AddItemToObject(MSJSObject, "loudness", cJSON_CreateNumber(buf->loudness));
					data = cJSON_Print(MSJSObject);
					cJSON_Delete(MSJSObject);
			}

			if(gn_put(&googlenest, googlenest_uri, data) == 0)
				printf("\n\rUpdate Sensor data to\n\r");
			free(data);
			gn_close(&googlenest);
		}
		else{
			printf("\n\rConnection failed!\n\r");
			break;
		}

		vTaskDelay(5 * configTICK_RATE_HZ);
	}
}
