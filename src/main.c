#include "FreeRTOS.h"
#include "task.h"
#include "diag.h"
#include "main.h"
#include "gpio_api.h"   // mbed
#include "gpio_irq_api.h"   // mbed
#include "device.h"
#include "semphr.h"
#include "inttypes.h"
#include "sleep_ex_api.h"

#include "wlan_intf.h"
#include <lwip/sockets.h>

#include "LSM9DS1.h"
#include "wifi_helper.h"

QueueHandle_t qh = 0;
xSemaphoreHandle wifiReadySema = NULL;
uint8_t useWifi = 1;
i2c_t i2c_obj;
gpio_t gpio_acc;

#define ACC_I2C_MTR_SDA    PB_3
#define ACC_I2C_MTR_SCL    PB_2
#define ACC_INT_PIN 			 PC_4

void configLSM9DS1() {
	LSM9DS1_configAccelInt(XHIE_XL, 0);
	LSM9DS1_configAccelThs(20, X_AXIS, 50, 0);
	LSM9DS1_configInt(INT1_CTRL, INT_IG_XL, INT_ACTIVE_LOW, INT_OPEN_DRAIN);
	// LSM9DS1_configInt(XG_INT1, INT_DRDY_XL | INT_DRDY_G, INT_ACTIVE_LOW, INT_PUSH_PULL);
	uint8_t val = LSM9DS1_getAccelIntSrc();
	rtl_printf("val %" PRIu8 "\n", val);
}

void gpio_acc_irq_handler(uint32_t id, gpio_irq_event event) {
	rtl_printf("acc int triggered\n");
}

void configInterrupts() {
	gpio_irq_t gpio_acc_int;

	gpio_irq_init(&gpio_acc_int, ACC_INT_PIN, gpio_acc_irq_handler, (uint32_t)(&gpio_acc));
	gpio_irq_set(&gpio_acc_int, IRQ_FALL, 1);   // Falling Edge Trigger
	gpio_irq_enable(&gpio_acc_int);
}

void sensorTask() {
	sensor_buffer buf;
	const TickType_t xDelay = 500/portTICK_PERIOD_MS;
  i2c_init(&i2c_obj, ACC_I2C_MTR_SDA, ACC_I2C_MTR_SCL);

	LSM9DS1_init(&i2c_obj, IMU_MODE_I2C, LSM9DS1_AG_ADDR(1), LSM9DS1_M_ADDR(1));
	LSM9DS1_initAccel();
	// LSM9DS1_initGyro();
	configLSM9DS1();

	while (1) {
		LSM9DS1_readAccel(&buf.accGX, &buf.accGY, &buf.accGZ);
		buf.accX = (((float) settings.accel.scale)/32768.0)*buf.accGX;
		buf.accY = (((float) settings.accel.scale)/32768.0)*buf.accGY;
		buf.accZ = (((float) settings.accel.scale)/32768.0)*buf.accGZ;

		rtl_printf("Sending: accX %3.2f, accY %3.2f, accZ %3.2f\n",
							 buf.accX, buf.accY, buf.accZ);

		if(!xQueueSend(qh, &buf, 1000)) {
    	puts("Failed to send item to queue within 500ms");
		}

		vTaskDelay(xDelay);
	}
}

void clntTask(void *sema) {
  sensor_buffer buf;
  int cnt = 0;
	uint8_t IPObtained = 0;
	const TickType_t xDelay = 1000/portTICK_PERIOD_MS;

  while(1) {
		if (!xSemaphoreTake(*((xSemaphoreHandle *)sema), 1/portTICK_RATE_MS) &&
			  !IPObtained) {
			rtl_printf("waiting for wifi\n");
			vTaskDelay(xDelay);
			continue;
		}
		IPObtained = 1;

    if(!xQueueReceive(qh, &buf, 1000)) {
        // puts("Nothing to receive item within 1000 ms");
    } else {
			rtl_printf("Receiving: accX %3.2f, accY %3.2f, accZ %3.2f\n",
								 buf.accX, buf.accY, buf.accZ);

      if (IPObtained && useWifi) {
        uploadSensorData(&buf, sizeof(sensor_buffer));
        // sendToFirebase(&buf);
        // if (cnt < 1) {
        //   sendNTPpacket();
        //   cnt++;
        // } else cnt = 0;
      }
    }
  } // End of while(1)
}

/**
  * @brief  Main program.
  * @param  None
  * @retval None
  */
void main(void) {
	if ( rtl_cryptoEngine_init() != 0 ) {
		DiagPrintf("crypto engine init failed\r\n");
	}
	gpio_init(&gpio_acc, ACC_INT_PIN);
	gpio_dir(&gpio_acc, PIN_INPUT);
	gpio_mode(&gpio_acc, PullUp);
	configInterrupts();

	qh = xQueueCreate(1, sizeof(sensor_buffer));

	vSemaphoreCreateBinary(wifiReadySema);
	xSemaphoreTake(wifiReadySema, 1/portTICK_RATE_MS);

	/* wlan intialization */
	#if defined(CONFIG_WIFI_NORMAL) && defined(CONFIG_NETWORK)
		wlan_network();
	#endif

	if (useWifi) {
		xTaskCreate(connectWiFiSema, "wifi task", configMINIMAL_STACK_SIZE*6, &wifiReadySema, tskIDLE_PRIORITY + 4, NULL);
	}

	xTaskCreate(sensorTask, "sensor task", configMINIMAL_STACK_SIZE*6, NULL, tskIDLE_PRIORITY + 4, NULL);
	xTaskCreate(clntTask, "client task", configMINIMAL_STACK_SIZE*8, &wifiReadySema, tskIDLE_PRIORITY + 4, NULL);

	/*Enable Schedule, Start Kernel*/
	#if defined(CONFIG_KERNEL) && !TASK_SCHEDULER_DISABLED
		#ifdef PLATFORM_FREERTOS
		vTaskStartScheduler();
		#endif
	#else
		RtlConsolTaskRom(NULL);
	#endif
}
