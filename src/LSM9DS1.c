#include "LSM9DS1.h"
#include "inttypes.h"

i2c_t *LSM6DS3_obj = NULL;
IMUSettings settings;
int16_t gx, gy, gz; // x, y, and z axis readings of the gyroscope
int16_t ax, ay, az; // x, y, and z axis readings of the accelerometer
int16_t mx, my, mz; // x, y, and z axis readings of the magnetometer
int16_t temperature; // Chip temperature
float gBias[3], aBias[3], mBias[3];
int16_t gBiasRaw[3], aBiasRaw[3], mBiasRaw[3];
uint8_t _mAddress, _xgAddress;
float gRes, aRes, mRes;
uint8_t _autoCalc;

void LSM9DS1_init(i2c_t *i2c_obj, interface_mode interface, uint8_t xgAddr, uint8_t mAddr) {
  LSM9DS1_initI2C(i2c_obj);
  settings.device.commInterface = interface;
  settings.device.agAddress = xgAddr;
  settings.device.mAddress = mAddr;

  settings.gyro.enabled = 0;
  settings.gyro.enableX = 0;
  settings.gyro.enableY = 0;
  settings.gyro.enableZ = 0;
  // gyro scale can be 245, 500, or 2000
  settings.gyro.scale = 245;
  // gyro sample rate: value between 1-6
  // 1 = 14.9    4 = 238
  // 2 = 59.5    5 = 476
  // 3 = 119     6 = 952
  settings.gyro.sampleRate = 6;
  // gyro cutoff frequency: value between 0-3
  // Actual value of cutoff frequency depends
  // on sample rate.
  settings.gyro.bandwidth = 0;
  settings.gyro.lowPowerEnable = 0;
  settings.gyro.HPFEnable = 0;
  // Gyro HPF cutoff frequency: value between 0-9
  // Actual value depends on sample rate. Only applies5-=[]
  // if gyroHPFEnable is true.
  settings.gyro.HPFCutoff = 0;
  settings.gyro.flipX = 0;
  settings.gyro.flipY = 0;
  settings.gyro.flipZ = 0;
  settings.gyro.orientation = 0;
  settings.gyro.latchInterrupt = 1;

  settings.accel.enabled = 1;
  settings.accel.enableX = 1;
  settings.accel.enableY = 1;
  settings.accel.enableZ = 1;
  // accel scale can be 2, 4, 8, or 16
  settings.accel.scale = 2;
  // accel sample rate can be 1-6
  // 1 = 10 Hz    4 = 238 Hz
  // 2 = 50 Hz    5 = 476 Hz
  // 3 = 119 Hz   6 = 952 Hz
  settings.accel.sampleRate = 4;
  // Accel cutoff freqeuncy can be any value between -1 - 3.
  // -1 = bandwidth determined by sample rate
  // 0 = 408 Hz   2 = 105 Hz
  // 1 = 211 Hz   3 = 50 Hz
  settings.accel.bandwidth = -1;
  settings.accel.highResEnable = 0;
  // accelHighResBandwidth can be any value between 0-3
  // LP cutoff is set to a factor of sample rate
  // 0 = ODR/50    2 = ODR/9
  // 1 = ODR/100   3 = ODR/400
  settings.accel.highResBandwidth = 0;

  settings.mag.enabled = 0;
  // mag scale can be 4, 8, 12, or 16
  settings.mag.scale = 4;
  // mag data rate can be 0-7
  // 0 = 0.625 Hz  4 = 10 Hz
  // 1 = 1.25 Hz   5 = 20 Hz
  // 2 = 2.5 Hz    6 = 40 Hz
  // 3 = 5 Hz      7 = 80 Hz
  settings.mag.sampleRate = 7;
  settings.mag.tempCompensationEnable = 0;
  // magPerformance can be any value between 0-3
  // 0 = Low power mode      2 = high performance
  // 1 = medium performance  3 = ultra-high performance
  settings.mag.XYPerformance = 3;
  settings.mag.ZPerformance = 3;
  settings.mag.lowPowerEnable = 0;
  // magOperatingMode can be 0-2
  // 0 = continuous conversion
  // 1 = single-conversion
  // 2 = power down
  settings.mag.operatingMode = 0;

  settings.temp.enabled = 1;
  for (int i=0; i<3; i++) {
    gBias[i] = 0;
    aBias[i] = 0;
    mBias[i] = 0;
    gBiasRaw[i] = 0;
    aBiasRaw[i] = 0;
    mBiasRaw[i] = 0;
  }
  _autoCalc = 0;
}

void LSM9DS1_initI2C(i2c_t *i2c_obj) {
  LSM6DS3_obj = i2c_obj;
}

void LSM9DS1_I2CwriteByte(uint8_t address, uint8_t data) {
  unsigned char dta_send[2];
  dta_send[0] = address;
  dta_send[1] = data;
  if (LSM6DS3_obj == NULL) {
    printf("NULL i2c obj!\n");
    return;
  }
  i2c_write(LSM6DS3_obj, LSM6DS3_DEVICE, &dta_send[0], 2, 1);
}

uint8_t LSM9DS1_I2CreadByte(uint8_t address) {
  if (LSM6DS3_obj == NULL) {
    printf("NULL i2c obj!\n");
    return 0;
  }
  uint8_t data;
  unsigned char dta_send[1];
  dta_send[0] = address;
  i2c_write(LSM6DS3_obj, LSM6DS3_DEVICE, &dta_send[0], 1, 1);
  i2c_read(LSM6DS3_obj, LSM6DS3_DEVICE, (char*)&data, 1, 1);
	return data;
}

uint8_t LSM9DS1_I2CreadBytes(uint8_t address, uint8_t buff[], uint8_t num) {
  if (LSM6DS3_obj == NULL) {
    printf("NULL i2c obj!\n");
    return 0;
  }
  unsigned char dta_send[1];
  dta_send[0] = address;
  i2c_write(LSM6DS3_obj, LSM6DS3_DEVICE, &dta_send[0], 1, 1);
  i2c_read(LSM6DS3_obj, LSM6DS3_DEVICE, (char*)&buff[0], num, 1);
	return num;
}

void LSM9DS1_initGyro() {
	uint8_t tempRegValue = 0;

	// CTRL_REG1_G (Default value: 0x00)
	// [ODR_G2][ODR_G1][ODR_G0][FS_G1][FS_G0][0][BW_G1][BW_G0]
	// ODR_G[2:0] - Output data rate selection
	// FS_G[1:0] - Gyroscope full-scale selection
	// BW_G[1:0] - Gyroscope bandwidth selection

	// To disable gyro, set sample rate bits to 0. We'll only set sample
	// rate if the gyro is enabled.
	if (settings.gyro.enabled)
	{
		tempRegValue = (settings.gyro.sampleRate & 0x07) << 5;
	}
	switch (settings.gyro.scale)
	{
		case 500:
			tempRegValue |= (0x1 << 3);
			break;
		case 2000:
			tempRegValue |= (0x3 << 3);
			break;
		// Otherwise we'll set it to 245 dps (0x0 << 4)
	}
	tempRegValue |= (settings.gyro.bandwidth & 0x3);
	LSM9DS1_I2CwriteByte(CTRL_REG1_G, tempRegValue);

	// CTRL_REG2_G (Default value: 0x00)
	// [0][0][0][0][INT_SEL1][INT_SEL0][OUT_SEL1][OUT_SEL0]
	// INT_SEL[1:0] - INT selection configuration
	// OUT_SEL[1:0] - Out selection configuration
	LSM9DS1_I2CwriteByte(CTRL_REG2_G, 0x00);

	// CTRL_REG3_G (Default value: 0x00)
	// [LP_mode][HP_EN][0][0][HPCF3_G][HPCF2_G][HPCF1_G][HPCF0_G]
	// LP_mode - Low-power mode enable (0: disabled, 1: enabled)
	// HP_EN - HPF enable (0:disabled, 1: enabled)
	// HPCF_G[3:0] - HPF cutoff frequency
	tempRegValue = settings.gyro.lowPowerEnable ? (1<<7) : 0;
	if (settings.gyro.HPFEnable)
	{
		tempRegValue |= (1<<6) | (settings.gyro.HPFCutoff & 0x0F);
	}
	LSM9DS1_I2CwriteByte(CTRL_REG3_G, tempRegValue);

	// CTRL_REG4 (Default value: 0x38)
	// [0][0][Zen_G][Yen_G][Xen_G][0][LIR_XL1][4D_XL1]
	// Zen_G - Z-axis output enable (0:disable, 1:enable)
	// Yen_G - Y-axis output enable (0:disable, 1:enable)
	// Xen_G - X-axis output enable (0:disable, 1:enable)
	// LIR_XL1 - Latched interrupt (0:not latched, 1:latched)
	// 4D_XL1 - 4D option on interrupt (0:6D used, 1:4D used)
	tempRegValue = 0;
	if (settings.gyro.enableZ) tempRegValue |= (1<<5);
	if (settings.gyro.enableY) tempRegValue |= (1<<4);
	if (settings.gyro.enableX) tempRegValue |= (1<<3);
	if (settings.gyro.latchInterrupt) tempRegValue |= (1<<1);
	LSM9DS1_I2CwriteByte(CTRL_REG4, tempRegValue);

	// ORIENT_CFG_G (Default value: 0x00)
	// [0][0][SignX_G][SignY_G][SignZ_G][Orient_2][Orient_1][Orient_0]
	// SignX_G - Pitch axis (X) angular rate sign (0: positive, 1: negative)
	// Orient [2:0] - Directional user orientation selection
	tempRegValue = 0;
	if (settings.gyro.flipX) tempRegValue |= (1<<5);
	if (settings.gyro.flipY) tempRegValue |= (1<<4);
	if (settings.gyro.flipZ) tempRegValue |= (1<<3);
	LSM9DS1_I2CwriteByte(ORIENT_CFG_G, tempRegValue);
}

void LSM9DS1_initAccel() {
	uint8_t tempRegValue = 0;

	//	CTRL_REG5_XL (0x1F) (Default value: 0x38)
	//	[DEC_1][DEC_0][Zen_XL][Yen_XL][Zen_XL][0][0][0]
	//	DEC[0:1] - Decimation of accel data on OUT REG and FIFO.
	//		00: None, 01: 2 samples, 10: 4 samples 11: 8 samples
	//	Zen_XL - Z-axis output enabled
	//	Yen_XL - Y-axis output enabled
	//	Xen_XL - X-axis output enabled
	if (settings.accel.enableZ) tempRegValue |= (1<<5);
	if (settings.accel.enableY) tempRegValue |= (1<<4);
	if (settings.accel.enableX) tempRegValue |= (1<<3);

	LSM9DS1_I2CwriteByte(CTRL_REG5_XL, tempRegValue);

	// CTRL_REG6_XL (0x20) (Default value: 0x00)
	// [ODR_XL2][ODR_XL1][ODR_XL0][FS1_XL][FS0_XL][BW_SCAL_ODR][BW_XL1][BW_XL0]
	// ODR_XL[2:0] - Output data rate & power mode selection
	// FS_XL[1:0] - Full-scale selection
	// BW_SCAL_ODR - Bandwidth selection
	// BW_XL[1:0] - Anti-aliasing filter bandwidth selection
	tempRegValue = 0;
	// To disable the accel, set the sampleRate bits to 0.
	if (settings.accel.enabled) {
		tempRegValue |= (settings.accel.sampleRate & 0x07) << 5;
	}
	switch (settings.accel.scale) {
		case 4:
			tempRegValue |= (0x2 << 3);
			break;
		case 8:
			tempRegValue |= (0x3 << 3);
			break;
		case 16:
			tempRegValue |= (0x1 << 3);
			break;
		// Otherwise it'll be set to 2g (0x0 << 3)
	}
	if (settings.accel.bandwidth >= 0) {
		tempRegValue |= (1<<2); // Set BW_SCAL_ODR
		tempRegValue |= (settings.accel.bandwidth & 0x03);
	}
	LSM9DS1_I2CwriteByte(CTRL_REG6_XL, tempRegValue);

	// CTRL_REG7_XL (0x21) (Default value: 0x00)
	// [HR][DCF1][DCF0][0][0][FDS][0][HPIS1]
	// HR - High resolution mode (0: disable, 1: enable)
	// DCF[1:0] - Digital filter cutoff frequency
	// FDS - Filtered data selection
	// HPIS1 - HPF enabled for interrupt function
	tempRegValue = 0;
	if (settings.accel.highResEnable) {
		tempRegValue |= (1<<7); // Set HR bit
		tempRegValue |= (settings.accel.highResBandwidth & 0x3) << 5;
	}
	LSM9DS1_I2CwriteByte(CTRL_REG7_XL, tempRegValue);
}

uint8_t LSM9DS1_getStatus() {
	uint8_t status = LSM9DS1_I2CreadByte(STATUS_REG_1);
	return status;
}

void LSM9DS1_readAccel(int16_t *accX, int16_t *accY, int16_t *accZ) {
	uint8_t temp[6]; // We'll read six bytes from the accelerometer into temp
	LSM9DS1_I2CreadBytes(OUT_X_L_XL, temp, 6); // Read 6 bytes, beginning at OUT_X_L_XL

	ax = (temp[1] << 8) | temp[0]; // Store x-axis values into ax
	ay = (temp[3] << 8) | temp[2]; // Store y-axis values into ay
	az = (temp[5] << 8) | temp[4]; // Store z-axis values into az

	if (_autoCalc) {
		ax -= aBiasRaw[X_AXIS];
		ay -= aBiasRaw[Y_AXIS];
		az -= aBiasRaw[Z_AXIS];
	}

  *accX = ax;
  *accY = ay;
  *accZ = az;
}

int16_t LSM9DS1_readAccel_axis(lsm9ds1_axis axis) {
	uint8_t temp[2];
	int16_t value;
	LSM9DS1_I2CreadBytes(OUT_X_L_XL + (2 * axis), temp, 2);
	value = (temp[1] << 8) | temp[0];

	if (_autoCalc)
		value -= aBiasRaw[axis];

	return value;
}

void LSM9DS1_readTemp() {
	uint8_t temp[2]; // We'll read two bytes from the temperature sensor into temp
	LSM9DS1_I2CreadBytes(OUT_TEMP_L, temp, 2); // Read 2 bytes, beginning at OUT_TEMP_L
	temperature = ((int16_t)temp[1] << 8) | temp[0];
}

void LSM9DS1_readGyro(uint16_t *gX, uint16_t *gY, uint16_t *gZ) {
	uint8_t temp[6]; // We'll read six bytes from the gyro into temp
	LSM9DS1_I2CreadBytes(OUT_X_L_G, temp, 6); // Read 6 bytes, beginning at OUT_X_L_G
	gx = (temp[1] << 8) | temp[0]; // Store x-axis values into gx
	gy = (temp[3] << 8) | temp[2]; // Store y-axis values into gy
	gz = (temp[5] << 8) | temp[4]; // Store z-axis values into gz
	if (_autoCalc)
	{
		gx -= gBiasRaw[X_AXIS];
		gy -= gBiasRaw[Y_AXIS];
		gz -= gBiasRaw[Z_AXIS];
	}

  *gX = gx;
  *gY = gy;
  *gZ = gz;
}

int16_t LSM9DS1_readGyro_axis(lsm9ds1_axis axis) {
	uint8_t temp[2];
	int16_t value;

	LSM9DS1_I2CreadBytes(OUT_X_L_G + (2 * axis), temp, 2);

	value = (temp[1] << 8) | temp[0];

	if (_autoCalc)
		value -= gBiasRaw[axis];

	return value;
}

void LSM9DS1_configInt(interrupt_select interrupt, uint8_t generator,
	                     h_lactive activeLow, pp_od pushPull) {
	// Write to INT1_CTRL or INT2_CTRL. [interupt] should already be one of
	// those two values.
	// [generator] should be an OR'd list of values from the interrupt_generators enum
	LSM9DS1_I2CwriteByte(interrupt, generator);

	// Configure CTRL_REG8
	uint8_t temp;
	temp = LSM9DS1_I2CreadByte(CTRL_REG8);

	if (activeLow) temp |= (1<<5);
	else temp &= ~(1<<5);

	if (pushPull) temp &= ~(1<<4);
	else temp |= (1<<4);

	LSM9DS1_I2CwriteByte(CTRL_REG8, temp);
}

void LSM9DS1_reset() {
  uint8_t temp;
	temp = LSM9DS1_I2CreadByte(CTRL_REG8);
  LSM9DS1_I2CwriteByte(CTRL_REG8, temp | 0x01);
}

void LSM9DS1_configInactivity(uint8_t duration, uint8_t threshold, uint8_t sleepOn) {
	uint8_t temp = 0;

	temp = threshold & 0x7F;
	if (sleepOn) temp |= (1<<7);
	LSM9DS1_I2CwriteByte(ACT_THS, temp);

	LSM9DS1_I2CwriteByte(ACT_DUR, duration);
}

uint8_t LSM9DS1_getInactivity() {
	uint8_t temp = LSM9DS1_I2CreadByte(STATUS_REG_0);
	temp &= (0x10);
	return temp;
}

void LSM9DS1_configAccelInt(uint8_t generator, uint8_t andInterrupts) {
	// Use variables from accel_interrupt_generator, OR'd together to create
	// the [generator]value.
	uint8_t temp = generator;
	if (andInterrupts) temp |= 0x80;
	LSM9DS1_I2CwriteByte(INT_GEN_CFG_XL, temp);
}

void LSM9DS1_configAccelThs(uint8_t threshold, lsm9ds1_axis axis, uint8_t duration, uint8_t wait) {
	// Write threshold value to INT_GEN_THS_?_XL.
	// axis will be 0, 1, or 2 (x, y, z respectively)
	LSM9DS1_I2CwriteByte(INT_GEN_THS_X_XL + axis, threshold);

	// Write duration and wait to INT_GEN_DUR_XL
	uint8_t temp;
	temp = (duration & 0x7F);
	if (wait) temp |= 0x80;
	LSM9DS1_I2CwriteByte(INT_GEN_DUR_XL, temp);
}

uint8_t LSM9DS1_getAccelIntSrc() {
	uint8_t intSrc = LSM9DS1_I2CreadByte(INT_GEN_SRC_XL);

	// Check if the IA_XL (interrupt active) bit is set
	if (intSrc & (1<<6)) {
		return (intSrc & 0x3F);
	}
	return 0;
}

void LSM9DS1_configGyroInt(uint8_t generator, uint8_t aoi, uint8_t latch) {
	// Use variables from accel_interrupt_generator, OR'd together to create
	// the [generator]value.
	uint8_t temp = generator;
	if (aoi) temp |= 0x80;
	if (latch) temp |= 0x40;
	LSM9DS1_I2CwriteByte(INT_GEN_CFG_G, temp);
}

void LSM9DS1_configGyroThs(int16_t threshold, lsm9ds1_axis axis, uint8_t duration, uint8_t wait) {
	uint8_t buffer[2];
	buffer[0] = (threshold & 0x7F00) >> 8;
	buffer[1] = (threshold & 0x00FF);
	// Write threshold value to INT_GEN_THS_?H_G and  INT_GEN_THS_?L_G.
	// axis will be 0, 1, or 2 (x, y, z respectively)
	LSM9DS1_I2CwriteByte(INT_GEN_THS_XH_G + (axis * 2), buffer[0]);
	LSM9DS1_I2CwriteByte(INT_GEN_THS_XH_G + 1 + (axis * 2), buffer[1]);

	// Write duration and wait to INT_GEN_DUR_XL
	uint8_t temp;
	temp = (duration & 0x7F);
	if (wait) temp |= 0x80;
	LSM9DS1_I2CwriteByte(INT_GEN_DUR_G, temp);
}

uint8_t LSM9DS1_getGyroIntSrc() {
	uint8_t intSrc = LSM9DS1_I2CreadByte(INT_GEN_SRC_G);

	// Check if the IA_G (interrupt active) bit is set
	if (intSrc & (1<<6))
	{
		return (intSrc & 0x3F);
	}

	return 0;
}
